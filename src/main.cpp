#include <windows.h>

#include "TSHooks.hpp"

extern bool frustrumCullerInit();
extern bool frustrumCullerDeinit();

extern bool shadowMapInit();
extern bool shadowMapDeinit();

//int numCulledScene, numCulledBricks;

BlFunctionDef(bool, , isShadowRenderPass);

//BlFunctionDef(void, __thiscall, SceneGraph__renderScene, ADDR, const unsigned int);
//BlFunctionHookDef(SceneGraph__renderScene);

//void __fastcall SceneGraph__renderSceneHook(ADDR obj, void *blank, const unsigned int mask)
//{
//	SceneGraph__renderSceneOriginal(obj, mask);
//	//BlPrintf("Scene: %d | Bricks: %d | Total: %d", numCulledScene, numCulledBricks, numCulledBricks + numCulledScene);
//}

bool init()
{
	BlInit;

	BlScanFunctionHex(isShadowRenderPass, "80 3D ? ? ? ? ? 74 18");

	//BlScanFunctionHex(SceneGraph__renderScene, "55 8D AC 24 ? ? ? ? 81 EC ? ? ? ? 6A FF 68 ? ? ? ? 64 A1 ? ? ? ? 50 81 EC ? ? ? ? A1 ? ? ? ? 33 C5 89 85 ? ? ? ? 53 56 57 50 8D 45 F4 64 A3 ? ? ? ? 64 A1 ? ? ? ?");
	//BlCreateHook(SceneGraph__renderScene);
	//BlTestEnableHook(SceneGraph__renderScene);

	if (!frustrumCullerInit()) return false;
	if (!shadowMapInit()) return false;

	BlPrintf("%s (v%s-%s): init'd", PROJECT_NAME, PROJECT_VERSION, TSFUNCS_DEBUG ? "debug":"release");
	return true;
}

bool deinit()
{
	//BlTestDisableHook(SceneGraph__renderScene);

	if (!frustrumCullerDeinit()) return false;
	if (!shadowMapDeinit()) return false;

	BlPrintf("%s: deinit'd", PROJECT_NAME);
	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch (reason) {
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
