#include <GL/gl.h>
#include <chrono>

#include "glm/gtc/type_ptr.hpp"
#include "glm/mat4x4.hpp"
#include "TSHooks.hpp"

static glm::mat4x4 projectionMatrix;
static ADDR gblGLMultMatrixfAddr,
            gShadowMapFrustumAddr;

BlFunctionDef(void, __thiscall, shadows_generalPrepForShadowMapRender, ADDR *);
BlFunctionHookDef(shadows_generalPrepForShadowMapRender);

//ISSUE: This hook seems to have no effect
//updateSplitDist() was inlined in r2033, so shadows_generalPrepForShadowMapRender must be used instead
void __fastcall shadows_generalPrepForShadowMapRenderHook(ADDR *thisPtr)
{
//#define F(i, j) *(*(frustums + i) + j)
#define F(i, j) *(frustums + 34*i + j)
	shadows_generalPrepForShadowMapRenderOriginal(thisPtr);

	float *frustums = (float *)gShadowMapFrustumAddr;
	for (int i = 0; i < 3; ++i) {
		F(i + 1, 0) = (F(i, 1) - F(i, 0))*0.1f;
	}
#undef F
}

//Used to easily grab the projection matrix in applyCropMatrix
void __stdcall glMultMatrixfHook(float *m)
{
	glMultMatrixf(m);
	projectionMatrix = *(glm::mat4x4 *)m;
}

BlFunctionDef(float, __fastcall, applyCropMatrix, ADDR *, float [16]);
BlFunctionHookDef(applyCropMatrix);

//ISSUE: This hook causes significant performance decrease
//       Might be due to the above issue
float __fastcall applyCropMatrixHook(ADDR *thisPtr, float mv[16])
{
	//Set the glMatrixf hook
	ADDR og = *(ADDR *)gblGLMultMatrixfAddr;
	*(ADDR *)gblGLMultMatrixfAddr = (ADDR)(&glMultMatrixfHook);

	float ret = applyCropMatrixOriginal(thisPtr, mv);

	//Restore the original function
	*(ADDR *)gblGLMultMatrixfAddr = og;

	ADDR thisAddr = (ADDR)thisPtr;

	float minX = *(float *)(thisAddr + 120);
	float minY = *(float *)(thisAddr + 128);

	float maxX = *(float *)(thisAddr + 124);
	float maxY = *(float *)(thisAddr + 132);

	//Controls the resolution of the shadows
	float scaleX = 2.0f/(maxX - minX);
	float scaleY = 2.0f/(maxY - minY);

	//Changing these at all breaks shadows
	float offsetX = -0.5f*(maxX + minX)*scaleX;
	float offsetY = -0.5f*(maxY + minY)*scaleY;

	glm::mat4x4 cropMatrix(scaleX, 0,      0, offsetX,
	                       0,      scaleY, 0, offsetY,
	                       0,      0,      1, 0,
	                       0,      0,      0, 1);

	cropMatrix = glm::transpose(cropMatrix);

	glLoadMatrixf(glm::value_ptr(cropMatrix));
	glMultMatrixf(glm::value_ptr(projectionMatrix));

	return ret;
}

bool shadowMapInit()
{
	ADDR glMultMatrixfCallAddr;
	BlScanHex(glMultMatrixfCallAddr, "FF 15 ? ? ? ? 56 FF 15 ? ? ? ? 83 C4 04 8D 84 24 ? ? ? ?");
	gblGLMultMatrixfAddr = *(ADDR *)(glMultMatrixfCallAddr + 2);

	ADDR shadowMapFrustumMovLoc;
	BlScanHex(shadowMapFrustumMovLoc, "BF ? ? ? ? DE E9 F3 0F 2A C0");
	gShadowMapFrustumAddr = *(ADDR *)(shadowMapFrustumMovLoc + 1);
	gShadowMapFrustumAddr -= 33;

	BlScanFunctionHex(shadows_generalPrepForShadowMapRender, "55 8B EC 83 E4 F8 83 EC 70 A1 ? ? ? ? 33 C4 89 44 24 6C A1 ? ? ? ?");
	BlScanFunctionHex(applyCropMatrix, "55 8B EC 83 E4 C0 81 EC ? ? ? ? A1 ? ? ? ? 33 C4 89 84 24 ? ? ? ? 53 56 57 8B F9 C7 44 24 ? ? ? ? ?");

	BlCreateHook(shadows_generalPrepForShadowMapRender);
	BlCreateHook(applyCropMatrix);

	BlTestEnableHook(shadows_generalPrepForShadowMapRender);
	BlTestEnableHook(applyCropMatrix);

	return true;
}

bool shadowMapDeinit()
{
	BlTestDisableHook(shadows_generalPrepForShadowMapRender);
	BlTestDisableHook(applyCropMatrix);

	return true;
}
