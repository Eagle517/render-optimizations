#pragma once

#include "glm/mat4x4.hpp"

class FrustrumCuller
{
private:
	glm::vec4 m_vvPlanes[6];
	glm::vec3 m_vvPoints[8];

public:
	void set(glm::mat4x4 &worldToClip);
	int testBoxVisibility(glm::vec3 aabb[2], float expand, int mask);
};
