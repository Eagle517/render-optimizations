#include <GL/gl.h>

#include "frustrum.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "TSHooks.hpp"

void FrustrumCuller::set(glm::mat4x4 &worldToClip)
{
	glm::mat4x4 clipToWorld = glm::inverse(worldToClip);

	const glm::vec4 clipCube[8] = { { -1.0f,  1.0f, -1.0f, 1.0f },   // 0 nul
	                                { -1.0f, -1.0f, -1.0f, 1.0f },   // 1 nll
	                                {  1.0f,  1.0f, -1.0f, 1.0f },   // 2 nur
	                                {  1.0f, -1.0f, -1.0f, 1.0f },   // 3 nlr
	                                { -1.0f,  1.0f,  1.0f, 1.0f },   // 4 ful
	                                { -1.0f, -1.0f,  1.0f, 1.0f },   // 5 fll
	                                {  1.0f,  1.0f,  1.0f, 1.0f },   // 6 fur
	                                {  1.0f, -1.0f,  1.0f, 1.0f } }; // 7 flr

	for (int i = 0; i < 8; ++i) {
		glm::vec4 vvPoint = clipToWorld * clipCube[i];
		m_vvPoints[i] = vvPoint / vvPoint[3];
	}

	const int pointsToPlane[6][4] = { { 0, 1, 3, 2 },   // Near
	                                  { 6, 7, 5, 4 },   // Far
	                                  { 4, 0, 2, 6 },   // Top
	                                  { 5, 1, 0, 4 },   // Left
	                                  { 7, 3, 1, 5 },   // Bottom
	                                  { 6, 2, 3, 7 } }; // Right

	auto SetPlane = [](const glm::vec3 &a, const glm::vec3 &b, const glm::vec3 &c, glm::vec4 *p) {
		glm::vec3 normal = glm::normalize(glm::cross(a - b, c - b));
		*p = glm::vec4(normal, -glm::dot(c, normal));
	};

	for (int i = 0; i < 6; ++i) {
		SetPlane(m_vvPoints[pointsToPlane[i][0]],
		         m_vvPoints[pointsToPlane[i][1]],
		         m_vvPoints[pointsToPlane[i][2]], &m_vvPlanes[i]);
	}
}

int FrustrumCuller::testBoxVisibility(glm::vec3 aabb[2], float expand, int mask)
{
	int retMask = 0;
	glm::vec3 minPoint, maxPoint;

	for (int i = 0; i < 6; ++i) {
		if (!(mask & (1 << i)))
			continue;

		if (m_vvPlanes[i].x > 0) {
			maxPoint.x = aabb[1].x;
			minPoint.x = aabb[0].x;
		}
		else {
			maxPoint.x = aabb[0].x;
			minPoint.x = aabb[1].x;
		}

		if (m_vvPlanes[i].y > 0) {
			maxPoint.y = aabb[1].y;
			minPoint.y = aabb[0].y;
		}
		else {
			maxPoint.y = aabb[0].y;
			minPoint.y = aabb[1].y;
		}

		if (m_vvPlanes[i].z > 0) {
			maxPoint.z = aabb[1].z;
			minPoint.z = aabb[0].z;
		}
		else {
			maxPoint.z = aabb[0].z;
			minPoint.z = aabb[1].z;
		}

		float maxDot = glm::dot(maxPoint, glm::vec3(m_vvPlanes[i]));
		float minDot = glm::dot(minPoint, glm::vec3(m_vvPlanes[i]));

		if (maxDot <= -(m_vvPlanes[i].w + expand))
			return -1;

		if (minDot <= -m_vvPlanes[i].w)
			retMask |= (1 << i);
	}

	int out;

	out = 0; for (int i = 0; i < 8; ++i) if (m_vvPoints[i].x > aabb[1][0]) ++out; else break; if (out == 8) return -1;
	out = 0; for (int i = 0; i < 8; ++i) if (m_vvPoints[i].x < aabb[0][0]) ++out; else break; if (out == 8) return -1;
	out = 0; for (int i = 0; i < 8; ++i) if (m_vvPoints[i].y > aabb[1][1]) ++out; else break; if (out == 8) return -1;
	out = 0; for (int i = 0; i < 8; ++i) if (m_vvPoints[i].y < aabb[0][1]) ++out; else break; if (out == 8) return -1;
	out = 0; for (int i = 0; i < 8; ++i) if (m_vvPoints[i].z > aabb[1][2]) ++out; else break; if (out == 8) return -1;
	out = 0; for (int i = 0; i < 8; ++i) if (m_vvPoints[i].z < aabb[0][2]) ++out; else break; if (out == 8) return -1;

	return retMask;
}

//BL HOOKS
BlFunctionDefExtern(bool, , isShadowRenderPass);

static ADDR gOctreeFrustrumCullerAddr,
            gFrustrumMask_octTree__renderFirstPassAddr,
            gFrustrumMask_fxBrickBatcher__drawOpaqueAddr,
            gFrustrumMask_SceneState__isObjectRenderedAddr,
            gForceShadowCull_SceneState__isObjectRenderedAddr1,
            gForceShadowCull_SceneState__isObjectRenderedAddr2,
            gForceShadowCull_octTree__processNodesAddr;

static FrustrumCuller OctreeCuller, SceneStateCuller;

BlFunctionDef(void, __thiscall, FrustrumCuller__init, ADDR *, void *);
BlFunctionHookDef(FrustrumCuller__init);

void __fastcall FrustrumCuller__initHook(ADDR *thisPtr, void *blank, void *state)
{
	glm::mat4x4 cameraMat, worldToClipMat;

	glGetFloatv(GL_MODELVIEW_MATRIX, glm::value_ptr(cameraMat));

	if (!isShadowRenderPass()) {
		glm::mat4x4 projMat;
		glGetFloatv(GL_PROJECTION_MATRIX, glm::value_ptr(projMat));
		worldToClipMat = projMat*cameraMat;
	}
	else
		worldToClipMat = cameraMat;

	if ((ADDR)thisPtr == gOctreeFrustrumCullerAddr)
		OctreeCuller.set(worldToClipMat);
	else
		SceneStateCuller.set(worldToClipMat);
}

BlFunctionDef(int, __thiscall, FrustrumCuller__testBoxVisility, ADDR *, glm::vec3 [2], int, float, float *);
BlFunctionHookDef(FrustrumCuller__testBoxVisility);

int __fastcall FrustrumCuller__testBoxVisilityHook(ADDR *thisPtr, void *blank, glm::vec3 box[2], int mask, float expand, float *outDist)
{
	if ((ADDR)thisPtr == gOctreeFrustrumCullerAddr)
		return OctreeCuller.testBoxVisibility(box, expand, mask);
	else
		return SceneStateCuller.testBoxVisibility(box, expand, mask);
}

bool frustrumCullerInit()
{
	ADDR octreeCullerMovAddr;
	BlScanHex(octreeCullerMovAddr, "B9 ? ? ? ? E8 ? ? ? ? 8B 0D ? ? ? ? 80 79 01 20");
	gOctreeFrustrumCullerAddr = *(ADDR *)(octreeCullerMovAddr + 1);

	BlScanHex(gFrustrumMask_octTree__renderFirstPassAddr, "6A 5F 8D 44 24 64");
	BlScanHex(gFrustrumMask_fxBrickBatcher__drawOpaqueAddr, "6A 5F 50 E8 ? ? ? ? 50 B9 ? ? ? ? E8 ? ? ? ?");
	BlScanHex(gFrustrumMask_SceneState__isObjectRenderedAddr, "6A 1F 05 ? ? ? ?");

	BlScanHex(gForceShadowCull_SceneState__isObjectRenderedAddr1, "75 35 80 B9 ? ? ? ? ?");
	BlScanHex(gForceShadowCull_SceneState__isObjectRenderedAddr2, "75 2C D9 05 ? ? ? ?");
	BlScanHex(gForceShadowCull_octTree__processNodesAddr, "74 0F 89 0C F5 ? ? ? ?");

	//Change these hardcoded masks to all be 63 (why?)
	BlPatchByte(gFrustrumMask_octTree__renderFirstPassAddr + 1, 0x3F);
	BlPatchByte(gFrustrumMask_fxBrickBatcher__drawOpaqueAddr + 1, 0x3F);
	BlPatchByte(gFrustrumMask_SceneState__isObjectRenderedAddr + 1, 0x3F);

	//Force shadows to be culled as well
	BlPatchBytes(2, gForceShadowCull_SceneState__isObjectRenderedAddr1, "\x90\x90"); //nop out these 2 if statements
	BlPatchBytes(2, gForceShadowCull_SceneState__isObjectRenderedAddr2, "\x90\x90");
	BlPatchByte(gForceShadowCull_octTree__processNodesAddr, 0xEB); //change jz to jmp to disregard result of isShadowPass

	BlScanFunctionHex(FrustrumCuller__init, "55 8B EC 83 E4 F8 81 EC ? ? ? ? A1 ? ? ? ? 33 C4 89 84 24 ? ? ? ? A1 ? ? ? ? 56");
	BlScanFunctionHex(FrustrumCuller__testBoxVisility, "83 EC 20 0F 57 C9");

	BlCreateHook(FrustrumCuller__init);
	BlCreateHook(FrustrumCuller__testBoxVisility);

	BlTestEnableHook(FrustrumCuller__init);
	BlTestEnableHook(FrustrumCuller__testBoxVisility);

	return true;
}

bool frustrumCullerDeinit()
{
	//restore masks to original values
	BlPatchByte(gFrustrumMask_octTree__renderFirstPassAddr + 1, 0x5F);
	BlPatchByte(gFrustrumMask_fxBrickBatcher__drawOpaqueAddr + 1, 0x5F);
	BlPatchByte(gFrustrumMask_SceneState__isObjectRenderedAddr + 1, 0x1F);

	//restore original code
	BlPatchBytes(2, gForceShadowCull_SceneState__isObjectRenderedAddr1, "\x75\x35");
	BlPatchBytes(2, gForceShadowCull_SceneState__isObjectRenderedAddr2, "\x75\x2C");
	BlPatchByte(gForceShadowCull_octTree__processNodesAddr, 0x74);

	BlTestDisableHook(FrustrumCuller__init);
	BlTestDisableHook(FrustrumCuller__testBoxVisility);

	return true;
}
