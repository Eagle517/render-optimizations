# Rendering Optimizations
This DLL provides several modifications that aim to increase overall rendering performance.  This is currently a WIP.

Credit to [Val](https://forum.blockland.us/index.php?topic=323819.0) for the original version.

## Usage
Use the latest version of [RedBlocklandLoader](https://gitlab.com/Eagle517/redblocklandloader/-/releases) to easily load the DLL into the game.

## Building
This DLL relies on [TSFuncs](https://gitlab.com/Eagle517/tsfuncs) which you will need to build first.  Building with CMake is straightforward, although it has only been setup for MinGW.  A toolchain has been included for cross-compiling on Linux.
