cmake_minimum_required(VERSION 3.0)
project(RenderOptimizations VERSION 1.0.0)

set(CMAKE_CXX_FLAGS "-static -static-libgcc -static-libstdc++ -Wall -Werror")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} -O2 -DTSFUNCS_DEBUG=false")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} -g -Og")

add_compile_definitions(PROJECT_NAME="${CMAKE_PROJECT_NAME}")
add_compile_definitions(PROJECT_EXPORT=${CMAKE_PROJECT_NAME})
add_compile_definitions(PROJECT_VERSION="${CMAKE_PROJECT_VERSION}")

set(CMAKE_CONFIGURATION_TYPES "Debug;Release" CACHE STRING "" FORCE)

if (NOT CMAKE_BUILD_TYPE MATCHES Release AND NOT CMAKE_BUILD_TYPE MATCHES Debug)
	set(CMAKE_BUILD_TYPE Release)
endif ()

if (DEFINED CMAKE_TOOLCHAIN_FILE AND NOT "${CMAKE_TOOLCHAIN_FILE}" STREQUAL "")
	message(STATUS "Toolchain: ${CMAKE_TOOLCHAIN_FILE}")
else ()
	message(STATUS "Toolchain: (None)")
endif ()
message(STATUS "Build type: ${CMAKE_BUILD_TYPE}")

find_package(TSFUNCS REQUIRED)

include_directories("${TSFUNCS_INCLUDE_DIRS};${CMAKE_SOURCE_DIR}/include")
link_directories(${TSFUNCS_LIBRARY_DIRS})

set(CMAKE_FIND_LIBRARY_PREFIXES "" "lib")
set(CMAKE_FIND_LIBRARY_SUFFIXES "" ".lib" ".a")

find_library(MINHOOK_LIB_RELEASE_LOC
	NAMES minhook libminhook minhook.x32 minhook.x64
	PATHS ${TSFUNCS_LIBRARY_DIRS}
	REQUIRED
)

find_library(MINHOOK_LIB_DEBUG_LOC
	NAMES minhookd libminhookd minhook.x32d minhook.x64d
	PATHS ${TSFUNCS_LIBRARY_DIRS}
	REQUIRED
)

if (CMAKE_BUILD_TYPE STREQUAL "Debug")
	set(TSFUNCS_LIB "tsfuncsd")
	set(MINHOOK_LIB_LOC ${MINHOOK_LIB_DEBUG_LOC})
else ()
	set(TSFUNCS_LIB "tsfuncs")
	set(MINHOOK_LIB_LOC ${MINHOOK_LIB_RELEASE_LOC})
endif ()

add_library(minhook STATIC IMPORTED)
set_target_properties(minhook PROPERTIES IMPORTED_LOCATION "${MINHOOK_LIB_LOC}")

add_library(${CMAKE_PROJECT_NAME} SHARED
	src/main.cpp
	src/frustrum.cpp
	src/shadowmap.cpp
)

set_target_properties(${CMAKE_PROJECT_NAME} PROPERTIES PREFIX "")

target_link_libraries(${CMAKE_PROJECT_NAME}
	${TSFUNCS_LIB}
	minhook
	psapi
	opengl32
)
